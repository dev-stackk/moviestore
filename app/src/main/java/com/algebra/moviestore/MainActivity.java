package com.algebra.moviestore;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText etTitle;
    private EditText etMainActor;
    private EditText etDirector;
    private EditText etYear;
    private EditText etRating;
    private EditText etSearch;
    private TextView tvResult;
    private Button bSave;
    private Button bSearch;
    private Button bAll;

    private Store store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        store = new Store();

        initWidgets();

        setupListeners();

    }

    private void setupListeners() {
        bSave.setOnClickListener(view -> {
            String title = etTitle.getText().toString();
            String mainActor = etMainActor.getText().toString();
            String director = etDirector.getText().toString();
            double rating = Double.parseDouble(etRating.getText().toString());
            int year = Integer.parseInt(etYear.getText().toString());

            Movie movie = new Movie(title, mainActor, director, year, rating);

            store.addMovie(movie);
            clearInputs();
        });

        bSearch.setOnClickListener(view -> {
            String searchTerm = etSearch.getText().toString();
            Movie movie = store.searchMovie(searchTerm);
            tvResult.setText(movie.toString());
            etSearch.setText("");
        });

        bAll.setOnClickListener(view -> {
            List<Movie> movies = store.getAllMovies();
            tvResult.setText(movies.toString());
        });
    }

    private void clearInputs() {
        etDirector.setText("");
        etRating.setText("");
        etMainActor.setText("");
        etTitle.setText("");
        etYear.setText("");
    }

    private void initWidgets() {
        etTitle = findViewById(R.id.etTitle);
        etMainActor = findViewById(R.id.etMainActor);
        etDirector = findViewById(R.id.etDirector);
        etYear = findViewById(R.id.etYear);
        etSearch = findViewById(R.id.etSearchTerm);
        etRating = findViewById(R.id.etRating);
        tvResult = findViewById(R.id.tvResult);
        bAll = findViewById(R.id.bShowAll);
        bSave = findViewById(R.id.bSave);
        bSearch = findViewById(R.id.bSearch);
    }
}
