package com.algebra.moviestore;

public class Movie {

    private String title;
    private String mainActor;
    private String director;
    private int year;
    private double rating;

    public Movie(String title, String mainActor, String director, int year, double rating) {
        this.title = title;
        this.mainActor = mainActor;
        this.director = director;
        this.year = year;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMainActor() {
        return mainActor;
    }

    public void setMainActor(String mainActor) {
        this.mainActor = mainActor;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return title + " " + director + " " + year + " " + mainActor + " " + rating + "\n";
    }
}
