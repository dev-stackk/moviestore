package com.algebra.moviestore;

import java.util.ArrayList;
import java.util.List;

public class Store {

    private List<Movie> movieList;

    public Store() {
        this.movieList = new ArrayList<>();
    }

    public void addMovie(Movie movie) {
        if (movieExists(movie)) {
            System.out.println("Not adding movie to store");
            return;
        }

        this.movieList.add(movie);
    }

    public Movie searchMovie(String title) {
        for (Movie movie : this.movieList) {
            if (movie.getTitle().equalsIgnoreCase(title)) {
                return movie;
            }
        }
        return null;
    }

    public List<Movie> getAllMovies() {
        return this.movieList;
    }

    private boolean movieExists(Movie movie) {
        String title = movie.getTitle();
        String director = movie.getDirector();

        for (Movie existingMovie : this.movieList) {
            if (existingMovie.getTitle().equalsIgnoreCase(title) && existingMovie.getDirector().equalsIgnoreCase(director)) {
                System.out.println("Movie already added to store");
                return true;
            }
        }
        return false;
    }

}
